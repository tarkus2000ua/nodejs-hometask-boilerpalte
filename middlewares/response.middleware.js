const responseMiddleware = (err, req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   
    if (err){
        // console.log('responseMiddleware');
        res.status(404).json({
            error: true,
            message: err.message
          });
    }
    else next();

}

exports.responseMiddleware = responseMiddleware;