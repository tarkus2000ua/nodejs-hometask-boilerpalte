const {
    fighter
} = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    MIN_DEFENSE = 1;
    MAX_DEFENSE = 10;
    MAX_POWER = 100;

    if (
        !(req.body.name &&
            req.body.health &&
            req.body.power &&
            req.body.defense)
    ) {
        res.status(400).json({
            error: true,
            message: 'Missing required fighter data'
        });
        return;
    };

    if (parseInt(req.body.defense) < MIN_DEFENSE || parseInt(req.body.defense) > MAX_DEFENSE) {
        res.status(400).json({
            error: true,
            message: 'Defense should be from 1 to 10'
        });
        return;
    }

    if (parseInt(req.body.power) >= MAX_POWER) {
        res.status(400).json({
            error: true,
            message: 'Power should be less than 100'
        });
        return;
    }

    if (req.body.id) {
        res.status(400).json({
            error: true,
            message: 'id is not allowed in the body of request'
        });
        return;
    }

    if (Object.keys(req.body).length > Object.keys(fighter).length - 1) {
        res.status(400).json({
            error: true,
            message: 'Request contains extra params'
        });
        return;
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    MIN_DEFENSE = 1;
    MAX_DEFENSE = 10;
    MAX_POWER = 100;

    if (
        !(req.body.name &&
            req.body.health &&
            req.body.power &&
            req.body.defense)
    ) {
        res.status(400).json({
            error: true,
            message: 'Missing required fighter data'
        });
        return;
    };

    if (parseInt(req.body.defense) < MIN_DEFENSE || parseInt(req.body.defense) > MAX_DEFENSE) {
        res.status(400).json({
            error: true,
            message: 'Defense should be from 1 to 10'
        });
        return;
    }

    if (parseInt(req.body.power) >= MAX_POWER) {
        res.status(400).json({
            error: true,
            message: 'Power should be less than 100'
        });
        return;
    }

    if (req.body.id) {
        res.status(400).json({
            error: true,
            message: 'id is not allowed in the body of request'
        });
        return;
    }

    if (Object.keys(req.body).length > Object.keys(fighter).length - 1) {
        res.status(400).json({
            error: true,
            message: 'Request contains extra params'
        });
        return;
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;