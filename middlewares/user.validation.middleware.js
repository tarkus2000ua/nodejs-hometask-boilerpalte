const {
    user
} = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const mailPattern = /^[\w.+\-]+@g(oogle)?mail\.com$/;
    const phonePattern = /^\+380\d{9}$/;
    const MIN_PASS_LENGTH = 3;

    if (
        !(req.body.email &&
            req.body.firstName &&
            req.body.lastName &&
            req.body.phoneNumber &&
            req.body.password)
    ) {
        res.status(400).json({
            error: true,
            message: 'Missing required user data'
        });
        return;
    };

    if (!mailPattern.test(req.body.email)) {
        res.status(400).json({
            error: true,
            message: 'Wrong email format'
        });
        return;
    }

    if (!phonePattern.test(req.body.phoneNumber)) {
        res.status(400).json({
            error: true,
            message: 'Wrong phone number format'
        });
        return;
    }

    if (req.body.password.length < MIN_PASS_LENGTH) {
        res.status(400).json({
            error: true,
            message: 'Password is too short'
        });
        return;
    }

    if (req.body.id) {
        res.status(400).json({
            error: true,
            message: 'id is not allowed in the body of request'
        });
        return;
    }

    if (Object.keys(req.body).length > Object.keys(user).length - 1) {
        res.status(400).json({
            error: true,
            message: 'Request contains extra params'
        });
        return;
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const mailPattern = /^[\w.+\-]+@g(oogle)?mail\.com$/;
    const phonePattern = /^\+380\d{9}$/;
    const MIN_PASS_LENGTH = 3;

    if (
        !(req.body.email &&
            req.body.firstName &&
            req.body.lastName &&
            req.body.phoneNumber &&
            req.body.password)
    ) {
        res.status(400).json({
            error: true,
            message: 'Missing required user data'
        });
        return;
    };

    if (!mailPattern.test(req.body.email)) {
        res.status(400).json({
            error: true,
            message: 'Wrong email format'
        });
        return;
    }

    if (!phonePattern.test(req.body.phoneNumber)) {
        res.status(400).json({
            error: true,
            message: 'Wrong phone number format'
        });
        return;
    }

    if (req.body.password.length < MIN_PASS_LENGTH) {
        res.status(400).json({
            error: true,
            message: 'Password is too short'
        });
        return;
    }

    if (req.body.id) {
        res.status(400).json({
            error: true,
            message: 'id is not allowed in the body of request'
        });
        return;
    }

    if (Object.keys(req.body).length > Object.keys(user).length - 1)  {
        res.status(400).json({
            error: true,
            message: 'Request contains extra params'
        });
        return;
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;