const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll(){
        return FighterRepository.getAll();
    }

    create(data){
        return FighterRepository.create(data);
    }

    delete(id){
        const item = FighterRepository.getOne(id);
        if (!item) {
            return null;
        } else return FighterRepository.delete(id);
    }
    
    update(id, dataToUpdate){
        const item = FighterRepository.getOne(id);
        if (!item) {
            return null;
        } else return FighterRepository.update(id, dataToUpdate);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();