const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll(){
        return UserRepository.getAll();
    }

    create(data){
        return UserRepository.create(data);
    }

    delete(id){
        const item = UserRepository.getOne(id);
        if (!item) {
            return null;
        } else return UserRepository.delete(id);
    }
    
    update(id, dataToUpdate){
        const item = UserRepository.getOne(id);
        if (!item) {
            return null;
        } else return UserRepository.update(id, dataToUpdate);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();