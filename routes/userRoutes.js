const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user


// GET /api/users
router.get('/', (req, res, next) => {
  res.json(UserService.getAll());
  next();
}, responseMiddleware);

//         GET /api/users/:id
router.get('/:id', (req, res, next) => {
  const result = UserService.search({id: req.params.id});
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error("User not found");
    next(err);
  }
}, responseMiddleware);

//         POST /api/users
router.post('/', createUserValid, (req, res, next) => {
  const result = UserService.create(req.body);
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error("User creation failed");
    next(err);
  }
}, responseMiddleware);

//         PUT /api/users/:id
router.put('/:id', updateUserValid, (req, res, next) => {
  const result = UserService.update({id: req.params.id}, req.body);
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error(`User with id of ${req.params.id} not found`);
    next(err);
  }
}, responseMiddleware);

//         DELETE /api/users/:id
router.delete('/:id', (req, res, next) => {
  const result = UserService.delete({id: req.params.id})
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error(`User with id of ${req.params.id} not found`);
    next(err);
  }
}, responseMiddleware);



module.exports = router;