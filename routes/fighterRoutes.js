const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
  res.json(FighterService.getAll());
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  const result = FighterService.search({id: req.params.id});
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error("Fighter not found");
    next(err);
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  const result = FighterService.create(req.body);
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error("Fighter creation failed");
    next(err);
  }
}), responseMiddleware;

router.put('/:id', updateFighterValid, (req, res, next) => {
  const result = FighterService.update({id: req.params.id}, req.body);
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error(`Fighter with id of ${req.params.id} not found`);
    next(err);
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  const result = FighterService.delete({id: req.params.id})
  if (result) {
    res.json(result);
    next();
  } else {
    const err = new Error(`Fighter with id of ${req.params.id} not found`);
    next(err);
  }
}, responseMiddleware);

module.exports = router;